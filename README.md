## Name
Alt-tech goggle for the brave search engine goggle feature.

## Description
Alt-tech goggle is an 'goggle' to be used for the goggle features in the brave search engine. Goggles is currently a new feature just launched (and in beta). Basically, it tries to re-rank results on top of existing brave search index. More info about it on [goggles whitepaper](https://brave.com/static-assets/files/goggles.pdf) and on [goggles github page](https://github.com/brave/goggles-quickstart)

This goggle promotes alt-tech platforms like odysee, minds etc while also promting privacy respecting front-ends of some platforms like libreddit (for reddit), piped (for youtube) etc.

The 'boost' level is kept at 2 (instead of usual 3) to avoid clogging up the front page with unneccesary extra links.
Mainstream (big tech) websites are delibreately not down ranked, becuase it will give users pretty boring search exprerince if they are downranked. To redirect big tech websites to their own front ends you can use extensions like [Privacy Redirect](https://github.com/SimonBrazell/privacy-redirect) and [Libredirect](https://libredirect.github.io/).

Some alt tech websites are deliberately de-ranked completely by goggle and bing index. So having boost=2 for such sites will 're-rank' them in their proper positions. While, boost=3 while slightly rank them higher than their re-rank position.

I will keep a watch if boost=2 is sufficient as preliminary test showed it is enough. Later on, I may increase the boost level.

## Installation
To use this particular goggle, take the URL of raw file https://gitlab.com/anonymous40/alt-tech-goggle/-/raw/master/alt-tech.goggle and paste it in https://search.brave.com/goggles/create. If all goes well, it will properly pass the URL; it it fails it will list the problem due to which it failed to pass it, due to a mistake from your end. 

## Usage
To Turn On this goggle (or any particular goggle) just click on 'Try this Goggle' or 'Follow' or click on the tick mark in the settings.

## Support
This goggle (or any goggle) may not work exactly the way you want. If so, try to make changes on individual level if you can. Goggles is currently in beta and I personally have seen the results pretty wierd (not good) with 'alt-tech goggle' turned on. The results from this goggle may or may not improve. It will probably depend on the actual brave search indexing alt-tech sites and various different frontends.

## Contributing
If you want to contribute to goggle you can do so by filling pull-requests (PR) or forking/copying this goggle and making necessary changes individually and creating your own indpendent goggle. Try to add regional (non-english) or less known alt-tech sites to this goggle or your own forked goggle.

## Author
Anonymous

## License
No particular license, use it in whatever way you want.
